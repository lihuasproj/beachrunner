﻿#pragma strict

function OnGUI()
{
    if(GUI.Button(new Rect(Screen.width/2.5f,Screen.height/3,Screen.width/5,Screen.height/10), "START OVER"))
    {
        Application.LoadLevel(2);
    }
    if (GUI.Button(new Rect(Screen.width / 2.5f, Screen.height / 2, Screen.width / 5, Screen.height / 10), "EXIT GAME"))
    {
        Application.Quit();
    } 
}