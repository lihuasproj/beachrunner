﻿#pragma strict

public var gameOver : boolean = false;
public var exeTime : float = 0.0f;
public var correctChoice : boolean = false;
public var incorrectChoice : boolean = false;

private var energy : int;
private var flag : boolean = false;
private var newEnergyValue : int = 250;
private var gameOverText : String = "GameOver";
private var distanceText : String = "Distance";
private var energyText : String = "Energy";
private var scoreText : String = "Score";
private var showUpText : boolean = false;
private var showDownText : boolean = false;
private var currTime : float = 0.0f;
private var timeToWait : float = 1.0f;

function Start () {
	energy = 1500;
	showUpText = false;
	showDownText = false;
    correctChoice = false;
    incorrectChoice = false;
}

function Update () {
	if (!gameOver)
		DecreaseEnergy();
	else if (!flag)
		Restart();
	Counter();
}

function OnGUI () {
	GUI.color = Color.cyan;
	GUI.skin.box.fontSize = 20;
	if(showUpText)
		GUI.Box(new Rect(Screen.width/2,Screen.height/2, 130, 35), "Power Up");
	if(showDownText)
		GUI.Box(new Rect(Screen.width/2,Screen.height/2, 150, 35), "Wrong Choice!");
	GUI.Box(new Rect(10, 10, 130, 35), energyText);
	//GUI.Box(new Rect(10, 10, 100, 25), distanceText);
	GUI.Box(new Rect(Screen.width/2.2f, 10, 150, 35), scoreText);
	GUI.Box(new Rect(Screen.width - 160, 10, 150, 35), distanceText);
	if (gameOver)
		GUI.Box(new Rect(Screen.width/2 - 75, Screen.height/2, 300, 35), gameOverText);
}

////////////////////////////////
function GetEnergy () {
	return energy;
}

function GetSocres () {
	return scoreText;
}

function GetDistance () {
	return distanceText;
}

function AddEnergy() {
	energy += newEnergyValue;
}

function DeductEnergy() {
	energy -= newEnergyValue;
}

function DecreaseEnergy() {
	energy -= Time.deltaTime;
	UpdateEnergy();
}

function UpdateEnergy() {
	energyText = "Energy: " + energy;
}
////////////////////////////////

function UpdateScores(scores : int) {
	 scoreText = "Scores: " + scores;
	 return scoreText;
}

function UpdateDistance(distance : int) {
	 distanceText = "Distance: " + distance;
}

function GameOver (text : String, f : boolean) {
	flag = f;
	gameOver = true; 
	gameOverText = text;
}

function Restart() {
 	yield WaitForSeconds (3.5);
 	Application.LoadLevel (0);
}

function Counter () {
	currTime = Time.time;
    if(correctChoice) {showUpText = true; }
    	else {showUpText = false;}
    if(incorrectChoice) {showDownText = true; }
    	else {showDownText = false;}
    if(exeTime != 0.0f){
	   if(currTime - exeTime > timeToWait) {
		  exeTime = 0.0f;
		  correctChoice = false;
		  incorrectChoice = false;
		}
	}
}
