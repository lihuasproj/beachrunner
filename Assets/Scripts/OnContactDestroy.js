﻿#pragma strict

public var explosion : GameObject;

private var GAME_LEVEL : int = 1;
private var scores : int = 0;
private var count : int = 0;
private var colors = new Array();
private var gameController : GameController;

function Start () {
	var gameControllerObject : GameObject = GameObject.FindWithTag ("GameController");
    if (gameControllerObject != null)
    {
        gameController = gameControllerObject.GetComponent (GameController);
    }
    if (gameController == null)
    {
        Debug.Log ("Cannot find 'GameController' script");
    }
}

function Swap(colors : Array, new_tag) {
	colors[0] = colors[1];
	colors[1] = new_tag;
	return colors;
}

function Classify(other_tag) {
	var tag : int;
	switch (other_tag) {
	case "blue":
		tag = 0;
		break;
	case "cyan":
		tag = 1;
		break;
	case "orange":
		tag = 2;
		break;
	case "pink":
		tag = 3;
		break;
	case "purple":
		tag = 4;
		break;
	}
	return tag;
}

function OnTriggerEnter(other : Collider) {
	gameController.exeTime = Time.time;
	if (other.tag == "Barrel") {
		Debug.Log("Barrel");
		// Instantiate explosion
		Instantiate(explosion, transform.position, transform.rotation);
		Destroy(gameObject);
		Destroy(other.gameObject);
		// Game over
		gameController.GameOver("You've hit a red barrel!", false);
	} 
	// Load different levles
	else if(other.tag == "end1") {
		Instantiate(explosion, transform.position, transform.rotation);
		yield WaitForSeconds (5);
		Application.LoadLevel(3); 
	}
	else {
		if (other.tag == "filler") 
			colors[count] = -1;
		else
			colors[count] = Classify(other.tag);
		if (count > GAME_LEVEL) {
			if ((colors[count] == colors[count-2]) && (colors[count] != -1)) {
				Debug.Log("Power up.");
				gameController.AddEnergy();
				Swap(colors, Classify(other.tag));
				gameController.correctChoice = true;
				scores += GAME_LEVEL + 1;
				gameController.UpdateScores(scores);
			}
		}
		count++;
		if(other.tag != "Untagged")
			Destroy(other.gameObject);
		Debug.Log(colors);
	}
	Debug.Log(colors);
}


