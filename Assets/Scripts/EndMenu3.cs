﻿using UnityEngine;
using System.Collections;

public class EndMenu3 : MonoBehaviour {

    void OnGUI()
    {
        GUI.Label(new Rect(Screen.width / 2.3f, Screen.height - Screen.height / 3.3f, Screen.width / 5, Screen.height / 10), "<color=black><size=50>Score</size></color>");
        if (GUI.Button(new Rect(Screen.width / 2.5f, Screen.height / 3.3f, Screen.width / 5, Screen.height / 10), "RESTART TO LEVEL 1"))
        {
            Application.LoadLevel(1);
        }
        if (GUI.Button(new Rect(Screen.width / 2.5f, Screen.height / 2.5f, Screen.width / 5, Screen.height / 10), "REPLAY LEVEL"))
        {
            Application.LoadLevel(3);
        }
        if (GUI.Button(new Rect(Screen.width / 2.5f, Screen.height / 1.8f, Screen.width / 5, Screen.height / 10), "MAIN MENU"))
        {
            Application.LoadLevel(0);
        }
    }
}

