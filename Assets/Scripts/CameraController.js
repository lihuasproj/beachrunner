﻿#pragma strict

public var astronaut : GameObject;
private var offset : Vector3;
private var rb : Rigidbody;
private var gameController : GameController;

function Start ()
{
	rb = GetComponent.<Rigidbody>();
    offset = transform.position - astronaut.transform.position;
    var gameControllerObject : GameObject = GameObject.FindWithTag ("GameController");
    if (gameControllerObject != null)
    {
        gameController = gameControllerObject.GetComponent (GameController);
    }
    if (gameController == null)
    {
        Debug.Log ("Cannot find 'GameController' script");
    }
}

function Update () {
//	if (gameController.gameOver) {
//		transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z-.1);
//    }
}

function LateUpdate ()
{
    transform.position = astronaut.transform.position + offset;
}
