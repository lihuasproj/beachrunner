﻿#pragma strict

var speed : float;
private var ORIGIN : float = -945;
private var rb : Rigidbody;
private var speedMul : float = 0.5;
private var gameController : GameController;

function Start () {
	rb = GetComponent.<Rigidbody>();
	var gameControllerObject : GameObject = GameObject.FindWithTag ("GameController");
    if (gameControllerObject != null)
    {
        gameController = gameControllerObject.GetComponent (GameController);
    }
    if (gameController == null)
    {
        Debug.Log ("Cannot find 'GameController' script");
    }
}

function Update () {
	gameController.UpdateDistance(ORIGIN - transform.position.z);
	if (!gameController.gameOver) {
		if (gameController.GetEnergy() > 0) {
			if (transform.position.z > -2330) {
				GetComponent.<Animation>().Play("run");
				rb.velocity.z = speed;
				if(Input.GetKey(KeyCode.A)) {
					transform.position = new Vector3(transform.position.x+.1, transform.position.y, transform.position.z);
				}
				if(Input.GetKey(KeyCode.D)) {
					transform.position = new Vector3(transform.position.x-.1, transform.position.y, transform.position.z);
				}
			}
			else {
				rb.velocity.z = 0;	
				GetComponent.<Animation>().Play("victory");
				gameController.GameOver("Congrats! You made it!", true);
				Debug.Log("Victory.");
			}
		}
		else {
			rb.velocity.z = 0;
			GetComponent.<Animation>().Play("death");
			gameController.GameOver("You've run out of energy!", false);
			Debug.Log("Run out of energy.");
		}
	}
}