#pragma strict

private var gameController : GameController;

function Start () {
	var gameControllerObject : GameObject = GameObject.FindWithTag ("GameController");
    if (gameControllerObject != null)
    {
        gameController = gameControllerObject.GetComponent (GameController);
    }
    if (gameController == null)
    {
        Debug.Log ("Cannot find 'GameController' script");
    }
}

function OnGUI()
{
	if (GUI.Button(new Rect(Screen.width / 2.5f, Screen.height / 3.3f, Screen.width / 5, Screen.height / 10), "GOTO NEXT LEVEL"))
	{
		Application.LoadLevel(4);
	}
	if (GUI.Button(new Rect(Screen.width / 2.5f, Screen.height / 2.5f, Screen.width / 5, Screen.height / 10), "REPLAY LEVEL"))
	{
		Application.LoadLevel(2);
	}
	if (GUI.Button(new Rect(Screen.width / 2.5f, Screen.height / 1.8f, Screen.width / 5, Screen.height / 10), "MAIN MENU"))
	{
		Application.LoadLevel(0);
	}
}